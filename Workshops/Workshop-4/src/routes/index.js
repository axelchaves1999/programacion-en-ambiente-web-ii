const express = require('express');
const router = express.Router();

const Course = require('../Models/courseModel');


router.get('/courses', async(req,res) =>{
    const courses = await Course.find();
    res.json(courses);
    
})

router.get('/course/:id',async(req,res)=>{
    const {id} = req.params;
    const course = await Course.findById({ _id: id});
    res.json(course);
})

router.post('/add', async(req, res) => {
    const course = new Course(req.body);
    await course.save();
    res.json(course);
})

router.get('/delete/:id', async(req, res) => {
    const { id } = req.params;
    await Course.deleteOne({ _id : id });
    res.send({Success: "Remove with success"})
})

router.patch('/edit/:id', async(req,res) =>{
    const {id} = req.params;
    Course.findById(id,function(err,course){
        if(err){
            res.json({error: "Couse does not exist"})
        }
        course.name = req.body.name ? req.body.name : course.name;
        course.code = req.body.code ? req.body.code : course.code;
        course.carrer = req.body.carrer ? req.body.carrer : course.carrer;
        course.credits = req.body.credits ? req.body.credits : course.credits;

        course.save();
        res.json(course)

    })
})


module.exports = router;