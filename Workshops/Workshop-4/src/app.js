const express = require('express');
const mongoose = require("mongoose");
const morgan = require('morgan');
const app = express();


//Conection with the database
mongoose.connect('mongodb://localhost/courses')
    .then(db => console.log('Db  connected'))
    .catch(err => console.log(err))


const indexRoutes = require('./routes/index');  
 

//middlewares
app.use(morgan('dev'));

const bodyParser = require("body-parser");
app.use(bodyParser.json());

// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));



//routes
app.use('/', indexRoutes)



app.listen(3000,() => console.log('App listen on port 3000'));