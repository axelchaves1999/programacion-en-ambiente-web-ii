let id = "";
getData();
function getData(){
    
    axios.get('http://localhost:3000/courses')
     .then(response =>{
        showData(response.data)
     })
     .catch(function(err){
         console.log(err);
     })
};

function showData(data){
    const tableBody = document.querySelector('.body');
    let count = 0;
    data.forEach(item =>{
        count++;
        id = item._id;
        tableBody.innerHTML += 
        `<tr>
        <th scope="row">${count}. </th>
        <td>${item.name}</td>
        <td>${item.code}</td>
        <td>${item.carrer}</td>
        <td>${item.credits}</td>
        <td>
        <button  onclick= deleteCourse("${item._id}") class="btn btn-danger" type="submit">Delete</button>
        <button  onclick= editCourse("${item._id}") class="btn btn-success" type="submit">Edit</button>
        </td>
      </tr>`;


    })
}

function deleteCourse(id) {
    axios.get(`http://localhost:3000/delete/${id}`)
    location.reload();
}

function editCourse(id){
    window.location = `http://isw711.com/Isw711/Workshops/Workshop-4/client/views/EditCourse.html?id=${id}`;
}
