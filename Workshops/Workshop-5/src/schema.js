module.exports =`
    type Course{
        _id: String!
        name: String
        code: String
        carrer: String
        credits: Int
    }

    type Query{
        allCourses: [Course!]!
    }

    type Mutation{
        createCourse(name: String!, code: String!, carrer: String!, credits: Int!): Course!
    }
`;