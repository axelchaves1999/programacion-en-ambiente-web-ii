const express = require('express');
const mongoose = require("mongoose");
const app = express();

mongoose.connect('mongodb://localhost/courses')
    .then(() => console.log('Db  connected'))
    .catch(err => console.log(err))


//models
const Course = require('./models/Courses')

const {graphqlExpress} = require('apollo-server-express'); 
const {makeExecutableSchema} = require('graphql-tools');

const typeDefs = require('./schema');
const resolvers = require('./resolvers')

//settings
app.set('port', process.env.PORT || 3000);


const schema = makeExecutableSchema({
    typeDefs,
    resolvers
})

//routes
app.use('/graphql', express.json(), graphqlExpress({
    schema,
    context:{
        Course
    }
}))




const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));


app.listen(app.get('port'), () => {
    console.log('server on port', app.get('port'));
});