module.exports = {
    Query:{
        allCourses: async(parent,args,{Courses})=>{
            const courses = await Courses.find();
            return courses.map(x =>{
                x._id = x._id.toString()
                return x;
            })
        }
    },
    Mutation:{
        createCourse: async(parent,args,{Courses})=>{
            const course = await new Courses(args).save();
            course._id = course._id.toString();
            return course;
        }
    }
}