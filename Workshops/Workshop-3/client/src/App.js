import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter,
  Card, CardText, CardBody,
  CardTitle, CardSubtitle
} from 'reactstrap';
import './assetss/css/App.css';

class App extends Component {



  constructor(props) {
    super(props);
    this.state = {
      items: [],
      breweries: 10
    }
  }



  componentDidMount() {
    fetch('https://api.openbrewerydb.org/breweries?per_page=10')
      .then(res => res.json())
      .then(json => {
        this.setState({

          items: json
        })
      });

  }



  render() {

    var { items } = this.state;


    return (
      <div className="container mt-5">
        <div className="row">


          {items.map(item => (


            <Card style={{ margin: '40px',  width: '300px' }} >
              <CardBody>
                <CardTitle tag="h5">{item.name}</CardTitle>
                <CardSubtitle tag="h6" className="mb-2 text-muted">{item.country}</CardSubtitle>
                <CardText>
                State: {item.state} <br/>
                City: {item.city} <br/>
               
                </CardText>
              </CardBody>
            </Card>


          ))}

        </div>
      </div>


    );
  }

}



export default App;
