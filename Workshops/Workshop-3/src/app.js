const numBeweries = 10;

searchBeweries();

function searchBeweries() {
    const url = `https://api.openbrewerydb.org/breweries?per_page=${numBeweries}`;


    fetch(url)
        .then(respond => respond.json())
        .then(result => {
            showBeweries(result);
            console.log(result);
        })
}

function showBeweries(breweries) {
    const container = document.querySelector('.row');
    breweries.forEach(item => {
        container.innerHTML += ` 
        <div class="card mt-2"  style="width: 18rem; ">
            <div class="card-body">
                <h5 class="card-title  text-center pb-2"> ${item.name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">Country: ${item.country}</h6>
                <p class="card-text">State: ${item.state}</p>
                <p class="card-text">City: ${item.city}</p>
               
            </div>
        </div>
    `;
    });

}