const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors({
    domains: '*',
    methods: "*"
}));

app.get('/hello', function(req, res) {
    res.send('Hello world');
})

app.listen(3000, () => console.log('Listen in port 3000'));