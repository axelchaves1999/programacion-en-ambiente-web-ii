const jwt = require('jsonwebtoken');
const { SECRETE_KEY } = require('../config');
const { AuthenticationError } = require('apollo-server')

module.exports = (context) => {
    //context = {...headers}
    const authHeader = context.req.headers.authorization;

    if (authHeader) {
        //Beare ....<-token
        const token = authHeader.split(' ')[1];
        if (token) {
            try {
             
                const user = jwt.verify(token, SECRETE_KEY);
                return user;
            } catch (err) {
                throw new AuthenticationError('Invalid/Expire token')
            }
        }

        throw new Error('Authentication token musb be \ Beare [token]')
    }
    throw new Error('Authentication token must be provided ')
}