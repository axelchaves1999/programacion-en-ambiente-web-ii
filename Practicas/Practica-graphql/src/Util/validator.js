module.exports.validateRegisterInput =(
    username,
    email,
    password,
    confirmPassword
)=>{
    const error = {};
    if(username.trim() == ''){
        error.username = 'Username must not be empty'
    }
    if(email.trim() == ''){
        error.username = 'Email must not be empty'
    }else{
        const regEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!email.match(regEx)){
            error.email = 'Email must be a valida email address'
        }
    }

    if(password === ''){
        error.password = 'Password must be not empty'
    }else if(password !== confirmPassword){
        error.confirmPassword = 'Passwords must match'
    }

    return {
        error,
        valid: Object.keys(error).length <1
    }
}

module.exports.valideteLoginInput = (username,password) =>{
    const error = {};
    if(username.trim() == ''){
        error.username = 'Username must not be empty'
    }

    if(password.trim() == ''){
        error.password = 'Password must not be empty'
    }

    return {
        error,
        valid: Object.keys(error).length <1
    }
}