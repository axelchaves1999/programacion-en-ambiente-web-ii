const {gql} = require('apollo-server');


module.exports = gql` 

    type Post{
        id: ID!
        body: String!
        createAt: String!
        username: String!
        comments: [Comment]!
        likes: [Like]!
    }

    type Comment{
        id: ID!
        createAt: String!
        username: String!
        body: String!
    }

    type Like{
        id: ID!
        createAt: String!
        username: String!
    }


    type User{
        id: ID!
        email: String!
        token: String!
        username: String!
        createAt: String!
    }

    input RegisterInput{
        username: String!
        password: String!
        confirmPassword: String!
        email: String!
    }

    type Query{
        getPosts: [Post]
        getUsers: [User]
        getPost(postId: ID!):Post
    }

    type Mutation{
        register(registerInput: RegisterInput): User!
        login(username: String!,password: String!): User!
        createAtPost(body: String!): Post!
        deletePost(postId: ID!): String!
        createComment(postId: ID!, body: String!):Post!
        deleteComment(postId: ID!, commentId: ID!): Post!
        likesPost(postId: ID!): Post!

    }
`