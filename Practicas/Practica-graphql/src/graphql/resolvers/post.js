const Post = require('../../Models/Post');
const checkAuth = require('../../util/crek-auth');
const { AuthenticationError } = require('apollo-server')
module.exports = {
    
    Query: {
        async getPosts() {
            try {
                const post = await Post.find().sort({createAt: -1});
                return post;
            } catch (err) {
                throw new Error(err);
            }
        },
        async getPost(_, { postId }) {
            try {
                const post = await Post.findById(postId);
                if (post) {
                    return post;
                } else {
                    throw new Error('Post no found');
                }

            } catch (err) {
                throw new Error(err);
            }

        }
    },

    Mutation: {
            //With context we can get the headers and the information inside
            async createAtPost(_, { body }, context) {
                const user = checkAuth(context);
            
                const newPost = new Post({
                    body,
                    user: user._id,
                    username: user.username,
                    createAt: new Date().toISOString()
                });

                const post = await newPost.save();
                return post;
            },

            async deletePost(_,{postId},context){
                const user = checkAuth(context);
                try{
                    const post = await Post.findById(postId);
                    if(user.username == post.username){
                        await post.delete();
                        return 'Post deleted successful';
                    }else{
                        throw new AuthenticationError('Action not allowed');
                    }
                } catch(err){
                    throw new Error(err);
                }
            }
        }

    
}
