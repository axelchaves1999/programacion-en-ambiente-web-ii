const User = require('../../Models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {SECRETE_KEY} = require('../../config');
const {UserInputError} = require('apollo-server');
const {validateRegisterInput,valideteLoginInput} = require('../../Util/validator');

function generateToken(user){
    
    return jwt.sign({
        id: user.id,
        email: user.email,
        username: user.username
    },SECRETE_KEY);

}


module.exports = {

    Query:{
        async getUsers(){
            try{
                const users = await User.find();
                return users;
            }catch(err){
                throw new Error(err);
            }
        }
    },


    Mutation:{

        async login(_,{username,password},context,info){
            const {error,valid} = valideteLoginInput(username,password);

            if(!valid){
                throw new UserInputError('Error',{error})
            }

            const user = await User.findOne({username});
            
            if(!user){
                error.general = 'User not found';
                throw new UserInputError('Wrong credential',{error})
            }

            const match = await bcrypt.compare(password,user.password);
            if(!match){
                error.general = 'Wrong credentials'
                throw new UserInputError('Wrong credentials',{error});
            }

            const token = generateToken(user);

            return {
                ...user._doc,
                //This is because in the document id is like _id
                id: user._id,
                token
            }

        },


        async register(_,{registerInput: {username,email,password,confirmPassword}},context,info){
            //TODO: Valite user data

            const {valid,error} = validateRegisterInput(username,email,password,confirmPassword);
            if(!valid){
                throw new UserInputError('Errors', {error})
            }
            //TODO: Makes sure user does not already exist

            const user = await User.findOne({username})
            if(user){
                throw new UserInputError('User is taken',{
                    error: {
                        username: 'This username is taken'
                    }
                })
            }

            password = await bcrypt.hash(password,12);

            const newUser = new User({
                email,
                username,
                password,
                createAt: new Date().toISOString()
            })
            const res = await newUser.save();

            const token = generateToken(res);
            return {
                ...res._doc,
                id: res._id,
                token
            }

        },

       
    }
}